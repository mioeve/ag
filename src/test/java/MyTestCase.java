import junit.framework.TestCase;

public class MyTestCase extends TestCase {

    protected ListTest lstTest = new ListTest();


    public void testSize() {
        assertEquals("Checking size of List", 0, lstTest.size());
    }

    public void testAdd() {
        lstTest.add("apple");
        assertEquals("Adding 1 more fruit to list", 1, lstTest.size());
    }

    public void testRemove() {
        lstTest.remove("apple");
        assertEquals("Removing 1 fruit from list", 0, lstTest.size());
    }}