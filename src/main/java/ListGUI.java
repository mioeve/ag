 import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;


public class ListGUI {

	private  JFrame  frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	ListTest a=new ListTest();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListGUI window = new ListGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ListGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 428);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAdd = new JLabel("Add:");
		lblAdd.setBounds(33, 68, 72, 18);
		frame.getContentPane().add(lblAdd);
		
		textField = new JTextField();
		textField.setBounds(98, 65, 86, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblRemove = new JLabel("Remove:");
		lblRemove.setBounds(33, 144, 72, 18);
		frame.getContentPane().add(lblRemove);
		
		textField_1 = new JTextField();
		textField_1.setBounds(98, 141, 86, 24);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblSize = new JLabel("Size:");
		lblSize.setBounds(33, 216, 72, 18);
		frame.getContentPane().add(lblSize);
		
		textField_2 = new JTextField();
		textField_2.setBounds(98, 213, 86, 24);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String fru= textField.getText();
			try {
			
				 a.add(fru);
			
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
                			
	
		});
		btnAdd.setBounds(217, 64, 113, 27);
		frame.getContentPane().add(btnAdd);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String rem= textField_1.getText();
				a.remove(rem);
			}
		});
		btnRemove.setBounds(217, 140, 113, 27);
		frame.getContentPane().add(btnRemove);
		
		JButton btnSize = new JButton("Size");
		btnSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			try {
				
				
					int si=a.size();
					String result=Integer.toString(si);
					textField_2.setText(result);
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
                			
	
		});
		btnSize.setBounds(217, 212, 113, 27);
		frame.getContentPane().add(btnSize);
		
		JButton btnShowFruits = new JButton("Show Fruits");
		btnShowFruits.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			try {
				
				 a.printAll();
				
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
                			
	
		});
		btnShowFruits.setBounds(128, 299, 148, 27);
		frame.getContentPane().add(btnShowFruits);
	}
}
